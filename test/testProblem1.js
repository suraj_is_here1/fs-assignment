const { createDirectory, createFile, deleteFile } = require("../problem1");

let dirPath = "./randomJsonFiles"
let numberOfFiles = 4;
function createAllFiles(numberOfFiles) {
    let promises = [];
    for (let fileCount = 0; fileCount < numberOfFiles; fileCount++) {
        promises.push(createFile(`${dirPath}/file-${fileCount}.json`));
    }
    return Promise.all(promises);
}
function deleteAllFiles(numberOfFiles) {
    let promises = [];
    for (let fileCount = 0; fileCount < numberOfFiles; fileCount++) {
        promises.push(deleteFile(`${dirPath}/file-${fileCount}.json`));
    }
    return Promise.all(promises);
}
function createAndDeleteFiles() {
    createDirectory(dirPath)
        .then((res) => {
            console.log(res);
            return createAllFiles(numberOfFiles);
        })
        .then((res) => {
            console.log(res);
            return deleteAllFiles(numberOfFiles);
        })
        .then((res) => {
            console.log(res);
        })
        .catch((err) => {
            console.log(err);
        });
}

try {
    createAndDeleteFiles()
} catch (err) {
    console.log(err.message);
}
