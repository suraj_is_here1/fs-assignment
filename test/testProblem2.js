const { readFile, deleteFile, writeFile, appendFile } = require('../problem2')

function multipleTask() {

    readFile('./lipsum.txt')
        .then((response) => {
            return writeFile('./uppertext.txt', response.toUpperCase())
        })
        .then((response) => {
            console.log(response);
            return writeFile('./nameOfAllFiles.txt', '')
        })
        .then((response) => {
            console.log(response);
            return appendFile('./nameOfAllFiles.txt', 'uppertext.txt,')
        })
        .then((response) => {
            console.log(response);
            return readFile('./uppertext.txt')
        })
        .then((response) => {
            return writeFile('./splittedtext.txt', JSON.stringify(response.split('. ')))
        })
        .then((response) => {
            console.log(response);
            return appendFile('./nameOfAllFiles.txt', 'splittedtext.txt,')
        })
        .then((response) => {
            console.log(response);
            return readFile('./splittedtext.txt')
        })
        .then((response) => {
            let sortedData = (JSON.parse(response).sort())
            return writeFile('./sortedtext.txt', JSON.stringify(sortedData))
        })
        .then((response) => {
            console.log(response);
            return appendFile('./nameOfAllFiles.txt', 'sortedtext.txt,')
        })
        .then((response) => {
            console.log(response);
            return readFile('./nameOfAllFiles.txt')
        })
        .then((response) => {
            let filenames = response.split(',').slice(0, -1)
            console.log('files created successfully', filenames);
            let deleteAllFiles = Promise.all(filenames.map(fileName => deleteFile(fileName)))
            return deleteAllFiles
        })
        .then((response) => {
            console.log(response);
        })
        .catch((err) => {
            console.log(err.message);
        })
}
try {
    multipleTask()
} catch (error) {
    console.log("Something went wrong!")
}
