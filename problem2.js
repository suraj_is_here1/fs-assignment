const fs = require('fs').promises;

function readFile(fileName) {
  return fs.readFile(fileName, 'utf-8')
}

function writeFile(fileName, data) {
  return fs.writeFile(fileName, data).then((res) => {
    if (res === undefined) {
      return `${fileName} created Successfully`
    }
    return res
  })
}
function appendFile(fileName, data) {
  return fs.appendFile(fileName, data).then((res) => {
    if (res === undefined) {
      return `${fileName} appended with ${data} Successfully`
    }
    return res
  })
}
function deleteFile(fileName) {
  return fs.unlink(fileName).then((res) => {
    if (res === undefined) {
      return `${fileName} deleted Successfully`
    }
    return res
  })
}

module.exports = { readFile, writeFile, deleteFile, appendFile }