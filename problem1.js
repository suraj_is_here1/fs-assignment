const fs = require('fs').promises;

function createDirectory(dirPath) {
  if (!dirPath) {
    throw new Error('Directory path cannot be an empty value')
  }
  return fs.mkdir(dirPath, { recursive: true }).then((res) => {
    if (res === undefined) {
      return 'Directory Created Successfully !'
    }
    return res
  })
}

function createFile(filePath) {
  return fs.writeFile(filePath, 'Hello').then((res) => {
    if (res === undefined) {
      return `${filePath} created Successfully !`
    }
    return res
  })

}
function deleteFile(filePath) {
  return fs.unlink(filePath).then((res) => {
    if (res === undefined) {
      return `${filePath} deleted Successfully !`
    }
    return res
  })
}

module.exports = { createDirectory, createFile, deleteFile }